#!/usr/bin/env bash

#set -x
set -a
set -e
set -u
set -o pipefail

# info commands
commands=(
"apt list --installed"
"ip a"
"zpool status"
"snap list"
"dpkg --get-selections"
"cat /etc/os-release"
"mount"
)

test -d info || mkdir info
for cmd in "${commands[@]}"
do
    out=$(echo $cmd | sed 's/ /_/g; s/\//_/g')
    $cmd > info/$out
done


