# rebuild_after_upgrade

Steps and cli used to recover installed packages and the rest after
upgrading to a new install

## Distro

I'll probably stick with ubuntu. It's been "good enough" for most of
my use cases.

## ZFS

I'm using zfs for boot partitions.  If the installer does that for us
then all the better.  I call my main zpool 'tank' and build other
filesystems out of that.






